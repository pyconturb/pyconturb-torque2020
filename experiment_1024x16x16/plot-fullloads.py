# -*- coding: utf-8 -*-
"""See MAE, ErrExt and ErrDELs for multiple loads
"""
import matplotlib.pyplot as plt
import pandas as pd


err_df = pd.read_csv('errors.csv', index_col=0)
patterns = ['nocon'] + [f'p{i}' for i in range(5)]
pkeys = {'nocon': 'No con', 'p0': 'B', 'p1': 'H', 'p2': 'BH', 'p3': 'BT', 'p4': 'BHT'}

i_om, i_pit, i_trq = 9, 3, 110
i_fbrm, i_ebrm = 37, 38
i_tbfa, i_tbss = 28, 29
i_shft = 36


for ifig, (ic, name, dl) in enumerate([(i_fbrm, 'Flap', 10),
                                       (i_tbfa, 'TB FA', 4),
                                       (i_shft, 'Shaft', 4)]):
    fig, axs = plt.subplots(3, 3, num=5+ifig, clear=True, figsize=(9, 8))

    for i, stat in enumerate(['merr', 'max', f'del{dl}']):
        if (name == 'Flap') and (stat == 'max'):
            stat = 'min'
        for j, wsp in enumerate([6, 12, 18]):
            ax = axs[i, j]
            if ('terr' in stat):
                case_df = err_df[stat]
            elif ('merr' in stat):
                case_df = err_df[f'{stat}_{ic}']
            else:
                case_df = err_df[f'{stat}err_{ic}']
            wsp_df = case_df.filter(regex=f'wsp{wsp}', axis=0)  # filter to that wsp
    
            # ax.plot([-0.5, 5.5], [0, 0], '--', c='0.75', zorder=-10, lw=0.5)
            ax.grid('on')
            for ip, p in enumerate(patterns):
                data = wsp_df.filter(regex=p, axis=0)
                ax.boxplot(data, positions=[ip])
                # vdict = ax.violinplot(data, positions=[ip], showmeans=True)
            ax.set_xticklabels([pkeys[p] for p in patterns])
            ax.set_xlim([-0.5, 5.5])
        
            ax.set_title(f'{name}, {stat}, {wsp}')
    plt.tight_layout()
