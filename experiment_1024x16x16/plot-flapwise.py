# -*- coding: utf-8 -*-
"""Figure 8, error stats for flapwise
"""
import os
import numpy as np
import pandas as pd
from _utils import make_torque_boxplot
from _inputs import i_fbrm, fig_dir


err_df = pd.read_csv('errors.csv', index_col=0)

num = 3
stats = [f'merr_{i_fbrm}', f'minerr_{i_fbrm}', f'del10err_{i_fbrm}']  # what stats to plot
ylabels = ['MAE Flap [MNm]', 'ErrExt Flap [MNm]','ErrDEL Flap [MNm]']
scales = [1e-3, 1e-3, 1e-3]

fig, ax = make_torque_boxplot(err_df, stats, ylabels, num=num, figsize=(8, 7),
                              scales=scales)

fig_name = os.path.basename(__file__).replace('.py', '.png')
fig.savefig(fig_dir + fig_name, dpi=150)
