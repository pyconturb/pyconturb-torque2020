# -*- coding: utf-8 -*-
"""Plot turbulence MAE for multiple components
"""
import matplotlib.pyplot as plt
import pandas as pd


err_df = pd.read_csv('errors.csv', index_col=0)
patterns = ['nocon'] + [f'p{i}' for i in range(5)]
pkeys = {'nocon': 'No con', 'p0': 'B', 'p1': 'H', 'p2': 'BH', 'p3': 'BT', 'p4': 'BHT'}

i_om, i_fbrm, i_ebrm = 9, 37, 38
i_tbfa, i_tbss = 28, 29
i_v0, i_v1, i_v2, i_v3, i_v4 = 14, 17, 20, 23, 26  

ic = None  # not used for terr

fig, axs = plt.subplots(3, 3, num=1, clear=True, figsize=(9, 8))

for i, c in enumerate('uvw'):
    for j, wsp in enumerate([6, 12, 18]):
        ax = axs[i, j]
        stat = f'terr_{c}'

        if ('terr' in stat):
            case_df = err_df[stat]
        elif ('merr' in stat):
            case_df = err_df[f'{stat}_{ic}']
        else:
            case_df = err_df[f'{stat}err_{ic}']
        wsp_df = case_df.filter(regex=f'wsp{wsp}', axis=0)  # filter to that wsp

        # ax.plot([-0.5, 5.5], [0, 0], '--', c='0.75', zorder=-10, lw=0.5)
        ax.grid('on')
        for ip, p in enumerate(patterns):
            data = wsp_df.filter(regex=p, axis=0)
            ax.boxplot(data, positions=[ip])
            # vdict = plt.violinplot(data, positions=[i], showmeans=True)
        ax.set_xticklabels([pkeys[p] for p in patterns])
        ax.set_xlim([-0.5, 5.5])
    
        ax.set_title(f'{stat}, {wsp}')
plt.tight_layout()
