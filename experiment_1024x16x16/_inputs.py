# -*- coding: utf-8 -*-
"""Script inputs such as turbulence box size, mean wind speed, etc.
"""
import numpy as np


# analysis inpts
nx, ny, nz = 1024, 16, 16  # low u for testing
wid_m1, ht_m1 = 180, 180  # 10-min simulation for DTU 10 MW
T, zhub = 600, 119  # mean wind speed, total time simulation, hub height
ae, l, gamma, seed, hf_comp, alpha = 1, 29.4, 3, 1001, True, 0.2
turb_dir = './turb'  # directory for turbulence boxes
htc_dir = '../simulations/htc'  # directory for turbulence boxes
old_path = f'../simulations/htc/base/DTU_10MW_RWT_turbonly_{nx}x{ny}x{nz}.htc'  # used for sanity check 1
patterns = {'p0': [29],
            'p1': [119],
            'p2': [29, 119],
            'p3': [29, 209],
            'p4': [29, 119, 209]}
##            'p4': np.array([29, 74, 119, 164, 209])}
con_dir = './constraints'  # directory for constraint files
con_seed = 1337  # seed for constrained turbulence
old_10mw_path = '../simulations/htc/base/DTU_10MW_RWT_turb.htc'
turb_class, ti_ref = 'A', 0.16  # turbulence class and reference turbulence intensity
nf_chunk = 100  # number of freqs in a chunk; increase for faster simulation
fig_dir = 'C:/Users/rink/Documents/publications/2019-12-13_torque2020_pyconturb/paper_r2/figures/'

# hawcs keys
i_om, i_pit, i_trq = 9, 3, 110
i_fbrm, i_ebrm = 37, 38
i_tbfa, i_tbss = 28, 29
i_v0, i_v1, i_v2, i_v3, i_v4 = 14, 17, 20, 23, 26
i_shft, i_ytlt = 36, 34

# reused intermediates
dy, dz = wid_m1/(ny-2), ht_m1/(nz-2)  # throw one row/column away so -2
wid, ht = dy*(ny-1), dz*(nz-1)  # full grid width and height
y_mid, z_mid = dy/2, zhub + dz/2  # height of center of turb box
y = np.linspace(-wid/2, wid/2, ny) + dy/2  # location of y points
z = np.linspace(-ht/2, ht/2, nz) + z_mid  # location of z points
f_samp = nx / T  # sonic sampling frequency
keys = [('nocon', 'No constraint'), ('p0', 'B'), ('p1', 'H'),
        ('p2', 'BH'), ('p3', 'BT'), ('p4', 'BHT')]
