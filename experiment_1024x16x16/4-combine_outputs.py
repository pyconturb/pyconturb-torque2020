# -*- coding: utf-8 -*-
"""Combine the custom outputs with values from wetb statistics
"""
import os
import pandas as pd


stats = ['max', 'min', 'mean', 'std', 'del4', 'del10']

# combine text files from hpc
print('Combining htc files')
err_df = pd.DataFrame()
out_files = sorted(os.listdir('outs'))
for i in range(len(out_files) // 2):
    df1 = pd.read_csv('outs/' + out_files[2*i], index_col=0)  # merr
    df2 = pd.read_csv('outs/' + out_files[2*i+1],
                      index_col=0).drop(['orig_name', 'pattern'], axis=1)  # terr
    df =  pd.concat((df1, df2), axis=1)  # should be 12 x 125
    err_df = pd.concat((err_df, df), axis=0)

# add column names for stats
nchan = df1.shape[1] - 2  # hardcode channels for convenience
cols = [f'{s}err_{i}' for s in stats for i in range(nchan)]
err_df = err_df.reindex(columns=list(err_df.columns) + cols)
del(df1, df2, df)

# load/convert from wetb statistics
print('Calculating errors from wetb output')
stat_df = pd.read_hdf('AN0003_statistics.h5', 'table').rename(
    columns={'m=4.0': 'del4', 'm=10.0': 'del10'})

# loop over filnames in err_df index
for fname in err_df.index:
    print(' processing ', fname)
    # original name
    oname = '_'.join(fname.split('_')[:2] + ['orig'])
    # get data
    rec_df = stat_df[stat_df['[case_id]'] == fname].sort_values('channel_nr', axis=0)
    orig_df = stat_df[stat_df['[case_id]'] == oname].sort_values('channel_nr', axis=0)
    # calculate errors and save to err_df
    for key in ['max', 'min', 'std', 'del4', 'del10']:
        err = orig_df[key].values - rec_df[key].values
        cols = [f'{key}err_{i}' for i in range(nchan)]
        err_df.loc[fname, cols] = err

# save final file
print('Saving')
out_path = './errors.pkl'
err_df.to_pickle(out_path)
