"""Generate the original turbulence boxes"""
import numpy as np
from pyconturb import gen_turb, gen_spat_grid 


from _inputs import (y, z, T, nx, turb_class, alpha, zhub, ti_ref, ny, nz)


def simulate_turbulence(htc_path):

    # get file
    htc_name = htc_path.split('/')[-1]
    split_name = htc_name.split('_')
    turb_name = 'turb_' + htc_name.replace('.htc', '')

    # get the wind speed and seed
    wsp = float(split_name[0].lstrip('wsp'))
    seed = int(split_name[1].lstrip('s'))
    tint = ti_ref*(0.75*wsp + 5.6) / wsp

    # generate the turbulence box and save it to the right folder
    kwargs = {'u_ref': wsp, 'T': T, 'dt': T/nx, 'seed': seed,
              'z_ref': zhub, 'alpha': alpha, 'turb_class': turb_class,
              'coh_model': '3d', 'nf_chunk': 200}
    # loop through components
    for ic, c in enumerate('uvw'):
        spat_df = gen_spat_grid(y, z, comps=[ic])
        # simulate turbulence
        sim_turb_df = gen_turb(spat_df, **kwargs)
        # reshape to array
        turb_arr = sim_turb_df.values.reshape(-1, ny, nz)
        # save that turbulence component box in HAWC2 format
        bin_path = './turb/' + turb_name + f'{c}.bin'
        with open(bin_path, 'wb') as bin_fid:
            turb_arr.astype(np.dtype('<f')).tofile(bin_fid)
        kwargs['seed'] = kwargs['seed'] + 1  # make sure u, v, w are not correlated


if __name__ == '__main__':
    # get list of htc files to analyse
    htc_list = np.loadtxt('./htclist.txt', dtype=str)
    orig_htc = [s for s in htc_list if 'orig' in s]
    for ih, htc_file in enumerate(orig_htc):
        print(f'Simulating {ih+1}/{len(orig_htc)} ({htc_file})...')
        simulate_turbulence(htc_file)
