# -*- coding: utf-8 -*-
"""Figure 6, MAE of operational data
"""
import os
import numpy as np
import pandas as pd
from _utils import make_torque_boxplot
from _inputs import i_om, i_pit, i_trq, fig_dir


err_df = pd.read_csv('errors.csv', index_col=0)

num = 2
stats = [f'merr_{i_om}', f'merr_{i_pit}', f'merr_{i_trq}']  # what stats to plot
ylabels = ['MAE RotSpd [rpm]', 'MAE Pitch [deg]','MAE GenTq [MNm]']
scales = [30/np.pi, 1, 1e-6]

fig, ax = make_torque_boxplot(err_df, stats, ylabels, num=num, figsize=(8, 7),
                              scales=scales)

fig_name = os.path.basename(__file__).replace('.py', '.png')
fig.savefig(fig_dir + fig_name, dpi=150)
