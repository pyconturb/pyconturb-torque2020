# -*- coding: utf-8 -*-
"""Utility functions for experiment
"""
from matplotlib import cm
import numpy as np
import matplotlib.pyplot as plt
from _inputs import keys


def make_torque_boxplot(df, stats, ylabels, legend=True, num=None,
                        figsize=None, ylabelpos=-0.3, scales=None):
    """Create the custom boxplot for this experimental data"""
    if scales is None:
        scales = np.ones(len(stats))
    pltprms = {'font.size': 11, 'axes.labelsize': 13}
    with plt.rc_context(pltprms):
        fig, axs = plt.subplots(len(stats), 3, num=num, clear=True,
                                figsize=figsize, squeeze=False)
        # useful variables
        box_sep = 0.12  # between boxes within wsp
        wsps = np.array([6, 12, 18])
        nk = len(keys)
        # get dark and light colors
        darks = cm.get_cmap('tab20')(range(1, 20, 2))
        lights = cm.get_cmap('tab20')(range(0, 20, 2))
        # make the plot
        bps = []
        for ist, stat in enumerate(stats):
            if ('terr' in stat) or ('merr' in stat):
                add_perc = True
            else:
                add_perc = False
            # filter data for that statistic
            stat_df = df[stat] * scales[ist]
            # loop thru wind speeds
            for iwsp, wsp in enumerate(wsps):
                ax = axs[ist, iwsp]
                wsp_df = stat_df.filter(regex=f'wsp{wsp}', axis=0)  # filter to that wsp
                for i, (key, name) in enumerate(keys):  # loop through patterns
                    data = wsp_df.filter(regex=key, axis=0)  # filter to pattern
                    if 'nocon' in key:
                        fc = '0.9'
                        ec = '0.6'
                    else:
                        fc = darks[i-1]
                        ec = lights[i-1]
                    bp = ax.boxplot(data, positions=[box_sep*i],
                                    widths=0.06, patch_artist=True,
                                    medianprops=dict(color=ec),
                                    boxprops=dict(facecolor=fc, color=ec),
                                    whiskerprops=dict(color=ec), capprops=dict(color=ec),
                                    flierprops=dict(markeredgecolor=ec, alpha=0.45))
                    if iwsp == 0:
                        bps.append(bp['boxes'][0])
                ax.set_xticks([])
                if ist == len(stats) - 1:
                    ax.set_xlabel(['Below rated', 'Near rated',
                                   'Above rated'][iwsp], labelpad=12)
                ax.spines['right'].set_visible(False)
                ax.spines['top'].set_visible(False)
                ax.set_xlim([-box_sep, nk*box_sep])
                axs[ist, 0].set_ylabel(ylabels[ist], labelpad=8)
                ax.get_yaxis().set_label_coords(ylabelpos, 0.5)
                if add_perc:
                    nocon_mean = np.mean(wsp_df.filter(regex='nocon', axis=0))
                    p4_mean = np.mean(wsp_df.filter(regex='p4', axis=0))
                    perr = (p4_mean - nocon_mean) / nocon_mean * 100
                    ax.annotate(f'{perr:.0f}%', (0.9, 0.9), xycoords='axes fraction',
                                ha='right', fontsize=12, c='0.1')
        axs[0, 0].legend(bps, [t[1] for t in keys],
                         bbox_to_anchor=(0.02, 1.10, 3.4, .102),
                         loc='lower left', ncol=6, mode='expand', borderaxespad=0.)
        plt.tight_layout()

    return fig, ax


def boxplot_stats(df, ichan, stat, ylabel, legend=True, axs=None, num=None):
    """Create the custom boxplot for this experimental data"""
    pltprms = {'font.size': 11, 'axes.labelsize': 13}
    with plt.rc_context(pltprms):
        if axs is None:  # axes don't exist
            fig, axs = plt.subplots(1, 3, num=num, clear=True, figsize=(8, 4))
        else:  # axes exist
            if num is not None:
                print('WARNING!!! Axes not passed, so num is ignored.')
            fig = axs[0].get_figure()
            for i in range(3):
                ax = axs[i]
                for item in ([ax.title] + ax.get_xticklabels() + ax.get_yticklabels()):
                    item.set_fontsize(pltprms['font.size'])
                ax.xaxis.get_label().set_fontsize(pltprms['axes.labelsize'])
                ax.yaxis.get_label().set_fontsize(pltprms['axes.labelsize'])
        # useful variables
        box_sep = 0.12  # between boxes within wsp
        wsps = np.array([6, 12, 18])
        nk = len(keys)
        # get dark and light colors
        darks = cm.get_cmap('tab20')(range(1, 20, 2))
        lights = cm.get_cmap('tab20')(range(0, 20, 2))
        # make the plot
        bps = []
        for iwsp, wsp in enumerate(wsps):
            ax = axs[iwsp]
            for i, (key, name) in enumerate(keys):
                filt_df = df[(df.key == key) & (df.channel_nr == ichan)
                             & (df.wsp == wsp)]
                if 'orig' in key:
                    fc = '0.7'
                    ec = '0.2'
                elif 'nocon' in key:
                    fc = '0.9'
                    ec = '0.6'
                else:
                    fc = darks[i-2]
                    ec = lights[i-2]
                bp = ax.boxplot(filt_df[stat].values, positions=[box_sep*i],
                                widths=0.1, patch_artist=True, whis=1e3,
                                medianprops=dict(color=ec),
                                boxprops=dict(facecolor=fc, color=ec),
                                whiskerprops=dict(color=ec), capprops=dict(color=ec),
                                flierprops=dict(markeredgecolor=ec))
                if iwsp == 0:
                    bps.append(bp['boxes'][0])
            ax.set_xticks([])
            ax.set_xlabel(['Below rated', 'Near rated', 'Above rated'][iwsp], labelpad=10)
            ax.spines['right'].set_visible(False)
            ax.spines['top'].set_visible(False)
            ax.set_xlim([-box_sep, nk*box_sep])
        axs[0].set_ylabel(ylabel, labelpad=8)
        axs[0].legend(bps, [t[1] for t in keys],  bbox_to_anchor=(0., 1.08, 3.91, .102),
                      loc='lower left', ncol=4, mode='expand', borderaxespad=0.)
        plt.tight_layout(w_pad=3)

    return fig, ax
