# -*- coding: utf-8 -*-
"""Fig. 9, time series and PSD of FBR bending moment
"""
import os
from matplotlib import cm
import numpy as np
from wetb.hawc2.Hawc2io import ReadHawc2
import matplotlib.pyplot as plt
from _inputs import i_fbrm, fig_dir


res_dir = './res/'
fnames = ['wsp6_s371_orig.sel', 'wsp6_s371_nocon_s1133.sel', 'wsp6_s371_p4_s292.sel']
colors = cm.get_cmap('tab20')(range(0, 20, 2))[[0, 1], :]
labels = ['Original', 'No constraint', 'BHT']

pltprms = {'font.size': 11, 'axes.labelsize': 12}
with plt.rc_context(pltprms):
    fig, axs = plt.subplots(1, 2, num=9, figsize=(10, 3.4), clear=True)
    
    ax1, ax2 = axs
    for i, fname in enumerate(fnames):
        data = ReadHawc2(res_dir + fname).ReadBinary()
        time = data[:, 0]
        sig = data[:, i_fbrm] / 1e3
        f = np.fft.rfftfreq(sig.size, (time[-1] - time[0])/time.size)
        mags = np.abs(np.fft.rfft(sig))
        ax1.plot(time, sig, c=colors[i-1] if i>0 else '0.2', lw=1, label=labels[i])
        # ax2.semilogy(f[:1000], mags[:1000],
        #              c=colors[i-1] if i>0 else '0.2')
        ax2.psd(sig, Fs=(time.size-1)/(time[-1] - time[0]), NFFT=sig.size,
                c=colors[i-1] if i>0 else '0.2', lw=1, label=labels[i])
    ax1.set_xlim([100, 600])
    ax1.set_xticks([])
    ax1.set_xlabel('Time [s]')
    ax1.set_ylabel('FBRM [MN]')
    ax2.set_xlim([0, 0.5])
    ax2.set_ylim([-30, 50])
    ax2.grid(False)
    plt.legend()
    plt.tight_layout()

fig_name = os.path.basename(__file__).replace('.py', '.png')
fig.savefig(fig_dir + fig_name, dpi=150)
