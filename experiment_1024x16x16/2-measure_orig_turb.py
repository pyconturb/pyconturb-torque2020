# -*- coding: utf-8 -*-
"""'Measure' the generated fine-mesh box to generate the constraint files
"""
import os
from vtmeas import MetMast, TurbBox
from vtmeas.instruments import SonicAnemometer
from _inputs import (y_mid, z_mid, nx, ny, nz, dy, dz, T, f_samp, con_dir, patterns,
                     turb_dir)


def measure_tbox(turb_file):
    wsp = float(turb_file.split('_')[1].lstrip('wsp'))
    dx = wsp*T/nx
    # load the turbulence box
    tb = TurbBox().read_hawc2(turb_dir + '/' + turb_file.replace('u.bin', '{c}.bin'),
                              ny=ny, nz=nz, dx=dx, dy=dy, dz=dz, y_mid=y_mid,
                              z_mid=z_mid, u_adv=wsp)

    # create constraint files from all of the patterns
    for ip, pk in enumerate(patterns):
        z_sonics = patterns[pk]
        sonic_anems = [SonicAnemometer([0, 0, z], f'{z}m SonAnem', f_samp)
                       for z in z_sonics]
        mm = MetMast(z_sonics[-1], instruments=sonic_anems)
        con_df = mm.measure(tb)
        con_df.to_csv(con_dir + '/' + turb_file.replace('origu.bin', f'{pk}.con').lstrip('turb_'))


if __name__ == '__main__':
    turb_files = [f for f in os.listdir('./turb') if f.endswith('u.bin')
                  and 'orig' in f]
    [measure_tbox(f) for f in turb_files]
