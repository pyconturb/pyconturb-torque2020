# -*- coding: utf-8 -*-
"""Use the constraint files to generate constrained turbulence boxes
"""
import os
import numpy as np
import pandas as pd
from pyconturb import gen_turb, gen_spat_grid, TimeConstraint
from _inputs import (con_dir, y, z, ny, nz, T, alpha, zhub, turb_class, nf_chunk, nx)


def gen_con_turb(htc_path):
    htc_name = htc_path.split('/')[-1]
    split_name = htc_name.split('_')
    wsp = float(split_name[0].lstrip('wsp'))
    seed = int(split_name[-1].lstrip('s').replace('.htc', ''))
    bin_path = './turb/turb_' + htc_name.replace('.htc', '{c}.bin')
    # if the file exists and is newer than the constrain file, skip it
    wturb_path = bin_path.replace('{c}', 'w')
    orig_path = './turb/turb_' + '_'.join(htc_name.split('_')[:2]) + '_origw.bin'
    if os.path.isfile(wturb_path): # file exists
        if (os.path.getmtime(wturb_path) > os.path.getmtime(orig_path)):  # turb newer
            return  # skip it

    # if nocon
    kwargs = {'u_ref': wsp, 'T': T, 'dt': T/nx,
              'seed': seed, 'z_ref': zhub, 'alpha': alpha, 'turb_class': turb_class,
              'coh_model': '3d', 'nf_chunk': nf_chunk}
    if 'nocon' in htc_name:
        con_tc = None
        interp_data = 'none'
    elif htc_path.startswith('./htc/p'):  # con turb
        con_name = '_'.join(htc_name.split('_')[:-1]) + '.con'
        con_tc = TimeConstraint(pd.read_csv(con_dir + '/' + con_name, index_col=0))
        con_tc.index = con_tc.index.map(lambda x: float(x) if (x not in 'kxyz') else x)
        interp_data = ['sig', 'spec']
    else:
        raise ValueError(f'Unexpected path "{htc_path}"!')
    # loop through components
    for ic, c in enumerate('uvw'):
        spat_df = gen_spat_grid(y, z, comps=[ic])

        # simulate turbulence
        sim_turb_df = gen_turb(spat_df, con_tc=con_tc, interp_data=interp_data,
                                **kwargs)

        # save that turbulence component box in HAWC2 format
        turb_arr = sim_turb_df.values.reshape(-1, ny, nz)
        with open(bin_path.replace('{c}', c), 'wb') as bin_fid:
            turb_arr.astype(np.dtype('<f')).tofile(bin_fid)
        kwargs['seed'] = kwargs['seed'] + 1  # make sure u, v, w are not correlated


if __name__ == '__main__':
    # get list of htc files to analyse
    htc_list = np.loadtxt('./htclist.txt', dtype=str)
    htcs = [s for s in htc_list if 'orig' not in s]
    # simulate the turbulence
    for ih, htc_file in enumerate(htcs):
        print(f'Simulating {ih+1}/{len(htcs)} ({htc_file})...')
        gen_con_turb(htc_file)
