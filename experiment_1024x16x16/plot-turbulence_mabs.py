# -*- coding: utf-8 -*-
"""Figure 5, MAE of turbulence
"""
import os
import pandas as pd
from _utils import make_torque_boxplot
from _inputs import fig_dir


err_df = pd.read_csv('errors.csv', index_col=0)


stats = ['terr_u']  # what stats to plot
num = 1
ylabels = ['MAE u [m/s]']

fig, ax = make_torque_boxplot(err_df, stats, ylabels, num=num, figsize=(8, 3.5))

fig_name = os.path.basename(__file__).replace('.py', '.png')
fig.savefig(fig_dir + fig_name, dpi=150)