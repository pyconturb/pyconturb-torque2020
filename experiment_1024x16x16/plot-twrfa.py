# -*- coding: utf-8 -*-
"""Figure 7, MAE of TBFA moment
"""
import os
import pandas as pd
from _utils import make_torque_boxplot
from _inputs import i_tbfa, fig_dir


err_df = pd.read_csv('errors.csv', index_col=0)

num = 4
stats = [f'merr_{i_tbfa}']  # what stats to plot
ylabels = ['MAE TwrBs FA [MNm]']
scales = [1e-3]

fig, ax = make_torque_boxplot(err_df, stats, ylabels, num=num, figsize=(8, 3.5),
                              scales=scales, ylabelpos=-0.25)

fig_name = os.path.basename(__file__).replace('.py', '.png')
fig.savefig(fig_dir + fig_name, dpi=150)
