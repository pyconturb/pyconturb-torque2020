# -*- coding: utf-8 -*-
"""Script inputs such as turbulence box size, mean wind speed, etc.
"""
import numpy as np


# analysis inpts
nx, ny, nz = 1024, 16, 16  # low u for testing
wid_m1, ht_m1 = 180, 180  # 10-min simulation for DTU 10 MW
U, T, zhub = 12, 600, 119  # mean wind speed, total time simulation, hub height
ae, l, gamma, seed, hf_comp, alpha = 1, 29.4, 3, 1001, True, 0.2
turb_dir = '../simulations/turb'  # directory for turbulence boxes
htc_dir = '../simulations/htc'  # directory for turbulence boxes
old_path = f'../simulations/htc/base/DTU_10MW_RWT_turbonly_{nx}x{ny}x{nz}.htc'  # used for sanity check 1
patterns = {'p0': [29],
            'p1': [119],
            'p2': [29, 119],
            'p3': [29, 209],
            'p4': [29, 119, 209]}
##            'p4': np.array([29, 74, 119, 164, 209])}
con_dir = '../simulations/constraints'  # directory for constraint files
con_seed = 1337  # seed for constrained turbulence
old_10mw_path = '../simulations/htc/base/DTU_10MW_RWT_turb.htc'
turb_class, ti_ref = 'A', 0.16  # turbulence class and reference turbulence intensity
nf_chunk = 100  # number of freqs in a chunk; increase for faster simulation
fig_dir = 'C:/Users/rink/Documents/publications/2019-12-13_torque2020_pyconturb/paper/figures'
times = np.array([190,  # p4 matches orig but not others. Non-con features big in upper rotor.
                  290,  # p0 and p3 match, but not p4.  Orig has burst in middle.
                  428,  # all cons match. Boring.
                  585,  # p0 matches worse than nocon...lol. adding the constraint changed the sample
                  ])
names = dict(p0='B', p1='H', p2='BH', p3='BT', p4='BHT')

# reused intermediates
dy, dz = wid_m1/(ny-2), ht_m1/(nz-2)  # throw one row/column away so -2
wid, ht = dy*(ny-1), dz*(nz-1)  # full grid width and height
y_mid, z_mid = dy/2, zhub + dz/2  # height of center of turb box
y = np.linspace(-wid/2, wid/2, ny) + dy/2  # location of y points
z = np.linspace(-ht/2, ht/2, nz) + z_mid  # location of z points
dx = U*T/nx  # longitudinal spacing
tint = ti_ref*(0.75*U + 5.6) / U  # turbulence intensity (class A)
# turb_name = (f'l{l:.2f}_ae{ae:.2f}_g{gamma:.2f}_{nx:.0f}x{ny:.0f}x{nz:.0f}_' +
#              f'{dx:.3f}x{dy:.3f}x{dz:.3f}_s{str(seed).zfill(4)}_ti{tint:.4f}_a{alpha}')
turb_name = (f'u{U:.0f}_{nx:.0f}x{ny:.0f}x{nz:.0f}_' +
             f'{dx:.3f}x{dy:.3f}x{dz:.3f}_s{str(seed).zfill(4)}_ti{tint:.4f}_a{alpha}')
turb_path = turb_dir + '/' + turb_name
f_samp = nx / T  # sonic sampling frequency
