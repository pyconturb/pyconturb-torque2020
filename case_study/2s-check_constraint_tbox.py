# -*- coding: utf-8 -*-
"""Sanity check: verify that the constraints match the points from the turbulence box

This checks that we are generating the constraints right, a.k.a. measuring the correct
point.
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from vtmeas import TurbBox
from _inputs import (turb_path, ny, nz, dx, dy, dz, y_mid, z_mid, U, con_dir)


# load a constraint file
pk = 'p3'  # top and bottom
con_df = pd.read_csv(con_dir + f'/con_{pk}.csv', index_col=0)

# load the turbulence box
tb = TurbBox().read_hawc2(turb_path + '_{c}.bin', ny=ny, nz=nz, dx=dx, dy=dy, dz=dz,
                          y_mid=y_mid, z_mid=z_mid, u_adv=U)

# compare the constraint to the turbulence box
iy = (tb.y.size-1)//2
for ip, iz in enumerate([0, -2]):  # iterate over top and bottom points
    wsp_con = con_df.iloc[4:, ip*3:(ip+1)*3].values
    wsp_tb = np.array([getattr(tb, s)[:, iy, iz] for s in 'uvw']).T
    fig = plt.figure(ip + 1, figsize=(8, 5))
    plt.clf()
    for ic, c in enumerate('uvw'):
        plt.subplot(3, 1, 1 + ic)
        plt.plot(tb.t, wsp_con[:, ic], label='Constraint')
        plt.plot(tb.t, wsp_tb[:, ic], label='TurbBox')
        plt.grid('on')
        plt.xlim([0, 600])
        if ic == 0:
            plt.legend()
    plt.tight_layout()
    #fig.savefig(f'../figures/sanity-check_con_tbox_p{ip}.png', dpi=100)
