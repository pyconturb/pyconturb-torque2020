# -*- coding: utf-8 -*-
"""Plot turbulence slices at a selection of times to gain insight into constrained
"""
from pathlib import Path
import matplotlib.pyplot as plt
import numpy as np
from vtmeas import TurbBox
from _inputs import (turb_dir, ny, nz, U, z_mid, y_mid, dx, dy, dz, con_seed, turb_name,
                     wid_m1, zhub, patterns, nx, T, fig_dir, times, names)


keys = ([('', 'Original'), (f'_nocon_s{con_seed}', 'No constraint')]
        + [(f'_p{i}_s{con_seed}', names[f'p{i}']) for i in range(5)])

# %% load the data

tbs = []
for ik, (key, name) in enumerate(keys):
    turb_path = turb_dir + '/' + turb_name + key + '_{c}.bin'
    tb = TurbBox().read_hawc2(turb_path, ny=ny, nz=nz, u_adv=U, z_mid=z_mid,
                              y_mid=y_mid, dx=dx, dy=dy, dz=dz)
    tbs.append(tb.u)
    print(f'{name}: {np.std(tb.u):.2f}')

# %% make the plot


# make the plot
for time in times:
    t = np.arange(nx)/nx*T
    it = np.argmin(np.abs(t-time))
    nk = len(keys)
    pltprms = {'font.size': 11, 'axes.labelsize': 13}
    with plt.rc_context(pltprms):
        fig, axs = plt.subplots(2, 4, num=1, clear=True, figsize=(10, 5))
    # load and plot the data
    imgs = []
    vmin, vmax = np.inf, -np.inf
    for ik, (key, name) in enumerate(keys):
        # load slice
        tb_slice = tbs[ik][it, :-1, :-1]

        # plot slice
        ax = axs[ik // 4, ik % 4]
        img = ax.imshow(tb_slice.T, origin='lower', vmin=vmin, vmax=vmax,
                        extent=[tb.y[0], tb.y[-2], tb.z[0], tb.z[-2]])
        # add rotor and constraint points
        ax.plot(wid_m1/2*np.cos(np.linspace(0, 2*np.pi, 201)),
                wid_m1/2*np.sin(np.linspace(0, 2*np.pi, 201)) + zhub, 'k:',
                label='Rotor')
        if '_p' in key:
            pk = key.split('_')[1]
            ax.plot(np.zeros(len(patterns[pk])), patterns[pk], 'o', mfc='none',
                    mec='#ff7f0e', alpha=0.7, ms=10, mew=2, clip_on=False,
                    label='Constraint')
        # update info for clim
        imgs.append(img)
        vmin = min(vmin, tb_slice.min())
        vmax = max(vmax, tb_slice.max())
        # prettify
        ax.set_title(name, fontsize=12, pad=10)
    # delete extra axis
    axs[-1, -1].set_visible(False)
    # set all clims
    [img.set_clim(vmin, vmax) for img in imgs]
    # prettify
    plt.tight_layout(rect=[0, 0, 0.95, 1], h_pad=1.25)
    axs[1, 2].legend(bbox_to_anchor=(1.55, 1.03, 1, 0), loc='upper left')
    cax = plt.axes([0.71, 0.079, 0.015, 0.367])
    plt.colorbar(imgs[2], cax=cax)
    fig.savefig(fig_dir + '/' + f'turb_slice_{time:.0f}.png', dpi=150)
