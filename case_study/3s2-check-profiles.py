# -*- coding: utf-8 -*-
"""Plot mean and standard deviation profiles
"""
import matplotlib.pyplot as plt
import numpy as np
from vtmeas import TurbBox
from _inputs import (turb_path, ny, nz, dx, dy, dz, y_mid, z_mid, U, con_seed, patterns,
                     wid_m1, zhub, nx)


load_keys = (['_{c}.bin', f'_nocon_s{con_seed}_' + '{c}.bin'] +
             [f'_{pt}_s{con_seed}_' + '{c}.bin' for pt in patterns])  # fine, nocon, then patterns
titles = ['Fine', 'No constraint'] + list(patterns)
nplot = len(load_keys)
plot_data = np.empty((nplot, nx, ny-1, nz-1))
for i, load_key in enumerate(load_keys):
    plot_data[i] = TurbBox().read_hawc2(turb_path + load_key, ny=ny,
                            nz=nz, dx=dx, dy=dy, dz=dz, y_mid=y_mid, z_mid=z_mid,
                            u_adv=U).u[:, :-1, :-1]

# mean
fig = plt.figure(2, figsize=(16, 9), clear=True, constrained_layout=False)
gs1 = fig.add_gridspec(nrows=1, ncols=3, left=0.05, right=0.95, bottom=0.55, top=0.95)
gs2 = fig.add_gridspec(nrows=1, ncols=4, bottom=0.05, top=0.5, left=0.05, right=0.95)
cmin, cmax = 8, 14

for ip in range(nplot):
    if ip <= 2:
        ax = fig.add_subplot(gs1[0, ip])
        im = ax.imshow(np.mean(plot_data[ip], axis=0).T, extent=[-90, 90, 29, 209],
                       origin='lower', vmin=cmin, vmax=cmax)
    else:
        ax = fig.add_subplot(gs2[0, ip-3])
        im = ax.imshow(np.mean(plot_data[ip], axis=0).T, extent=[-90, 90, 29, 209],
                       origin='lower', vmin=cmin, vmax=cmax)
    if ip == 0:
        plt.colorbar(im)
    if ip >= 2:
        zpatt = patterns[titles[ip]]
        ax.plot(np.zeros(len(zpatt)), zpatt, 'o', mew=2, mfc='none', mec='r', ms=11,
                clip_on=False, zorder=20)

# standard deviation
fig = plt.figure(3, figsize=(16, 9), clear=True, constrained_layout=False)
gs1 = fig.add_gridspec(nrows=1, ncols=3, left=0.05, right=0.95, bottom=0.55, top=0.95)
gs2 = fig.add_gridspec(nrows=1, ncols=4, bottom=0.05, top=0.5, left=0.05, right=0.95)
cmin, cmax = 1.6, 2.6

for ip in range(nplot):
    if ip <= 2:
        ax = fig.add_subplot(gs1[0, ip])
        im = ax.imshow(np.std(plot_data[ip], axis=0).T, extent=[-90, 90, 29, 209],
                       origin='lower', vmin=cmin, vmax=cmax)
    else:
        ax = fig.add_subplot(gs2[0, ip-3])
        im = ax.imshow(np.std(plot_data[ip], axis=0).T, extent=[-90, 90, 29, 209],
                       origin='lower', vmin=cmin, vmax=cmax)
    if ip == 0:
        plt.colorbar(im)
    if ip >= 2:
        zpatt = patterns[titles[ip]]
        ax.plot(np.zeros(len(zpatt)), zpatt, 'o', mew=2, mfc='none', mec='r', ms=11,
                clip_on=False, zorder=20)

# error
fig = plt.figure(4, figsize=(16, 9), clear=True, constrained_layout=False)
gs1 = fig.add_gridspec(nrows=1, ncols=3, left=0.05, right=0.95, bottom=0.55, top=0.95)
gs2 = fig.add_gridspec(nrows=1, ncols=4, bottom=0.05, top=0.5, left=0.05, right=0.95)
cmin, cmax = 0, 3

for ip in range(nplot):
    if ip == 0:
        ax = fig.add_subplot(gs1[0, ip])
        im = ax.imshow(np.mean(plot_data[ip], axis=0).T,
                       extent=[-90, 90, 29, 209], origin='lower', vmin=8,
                       vmax=14)
    elif ip <= 2:
        ax = fig.add_subplot(gs1[0, ip])
        im = ax.imshow(np.mean(np.abs(plot_data[ip] - plot_data[0]), axis=0).T,
                       extent=[-90, 90, 29, 209], origin='lower', vmin=cmin,
                       vmax=cmax)
    else:
        ax = fig.add_subplot(gs2[0, ip-3])
        im = ax.imshow(np.mean(np.abs(plot_data[ip] - plot_data[0]), axis=0).T,
                       extent=[-90, 90, 29, 209], origin='lower', vmin=cmin,
                       vmax=cmax)
    if ip == 0:
        plt.colorbar(im)
    if ip >= 2:
        zpatt = patterns[titles[ip]]
        ax.plot(np.zeros(len(zpatt)), zpatt, 'o', mew=2, mfc='none', mec='r', ms=11,
                clip_on=False, zorder=20)
