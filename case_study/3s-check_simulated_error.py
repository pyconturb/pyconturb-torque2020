# -*- coding: utf-8 -*-
"""Sanity check: verify that the sim'd conturb has zero error at constraints
"""
import matplotlib.pyplot as plt
import numpy as np
from vtmeas import TurbBox

# script inputs
from _inputs import (ny, nz, dx, dy, dz, U, turb_path, y_mid, z_mid, con_seed, patterns,
                     y, z)

vmaxs = [18, 7, 3]  # max of colorbars
# load the original turbulence
tb = TurbBox().read_hawc2(turb_path + '_{c}.bin', ny=ny, nz=nz, dx=dx, dy=dy, dz=dz,
                          y_mid=y_mid, z_mid=z_mid, u_adv=U)

# plot no con
pk = 'nocon'

# load the simulated constrained turbulence
tb_con = TurbBox().read_hawc2(turb_path + f'_{pk}_s{con_seed}_' + '{c}.bin', ny=ny,
                              nz=nz, dx=dx, dy=dy, dz=dz, y_mid=y_mid, z_mid=z_mid,
                              u_adv=U)

# calculate and plot the errors
plt.figure(1, figsize=(11, 4))
plt.clf()
for ic, c in enumerate('uvw'):
    ase_err = np.mean((getattr(tb_con, c) - getattr(tb, c))**2, axis=0)
    plt.subplot(1, 3, ic + 1)
    plt.contourf(y, z, ase_err.T, vmin=0, vmax=vmaxs[ic])
    plt.colorbar()
plt.suptitle(pk)
plt.tight_layout()

# loop through the patterns
for ip, pk in enumerate(patterns):

    # load the simulated constrained turbulence
    tb_con = TurbBox().read_hawc2(turb_path + f'_{pk}_s{con_seed}_' + '{c}.bin', ny=ny,
                                  nz=nz, dx=dx, dy=dy, dz=dz, y_mid=y_mid, z_mid=z_mid,
                                  u_adv=U)

    # calculate and plot the errors
    plt.figure(ip + 2, figsize=(11, 4))
    plt.clf()
    for ic, c in enumerate('uvw'):
        ase_err = np.mean((getattr(tb_con, c) - getattr(tb, c))**2, axis=0)
        plt.subplot(1, 3, ic + 1)
        plt.contourf(y, z, ase_err.T, vmin=0, vmax=vmaxs[ic])
        plt.colorbar()
    plt.suptitle(pk)
    plt.tight_layout()
