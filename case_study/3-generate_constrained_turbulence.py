# -*- coding: utf-8 -*-
"""Use the constraint files to generate constrained turbulence boxes
"""
import numpy as np
import pandas as pd
from pyconturb import gen_turb, gen_spat_grid, TimeConstraint
from _inputs import (con_dir, y, z, ny, nz, U, T, turb_path, patterns, con_seed, alpha,
                     zhub, tint, turb_class, nf_chunk)


# loop through patterns
for pk in patterns:

    # load the time constraint
    con_tc = TimeConstraint(pd.read_csv(con_dir + f'/con_{pk}.csv', index_col=0))
    con_tc.index = con_tc.index.map(lambda x: float(x) if (x not in 'kxyz') else x)

    # other inputs to gen_turb
    interp_data = ['sig', 'spec']
    kwargs = {'u_ref': U, 'T': T, 'dt': con_tc.get_time().index[1], 'seed': con_seed,
              'z_ref': zhub, 'alpha': alpha, 'coh_model': '3d',
              'nf_chunk': nf_chunk}

    # loop through components
    for ic, c in enumerate('uvw'):
        spat_df = gen_spat_grid(y, z, comps=[ic])

        # simulate turbulence
        sim_turb_df = gen_turb(spat_df, con_tc=con_tc, interp_data=interp_data, **kwargs)

        # save that turbulence component box in HAWC2 format
        bin_path = turb_path + f'_{pk}_s{con_seed}_{c}.bin'
        turb_arr = sim_turb_df.values.reshape(-1, ny, nz)
        with open(bin_path, 'wb') as bin_fid:
            turb_arr.astype(np.dtype('<f')).tofile(bin_fid)
        kwargs['seed'] = kwargs['seed'] + 1  # make sure u, v, w are not correlated

# generate the no-constraint box
kwargs = {'u_ref': U, 'T': T, 'dt': con_tc.get_time().index[1], 'seed': con_seed,
          'z_ref': zhub, 'alpha': alpha, 'turb_class': turb_class,
          'coh_model': '3d', 'nf_chunk': nf_chunk}
interp_data = 'none'

# loop through components
for ic, c in enumerate('uvw'):
    spat_df = gen_spat_grid(y, z, comps=[ic])

    # simulate turbulence
    sim_turb_df = gen_turb(spat_df, interp_data=interp_data, **kwargs)

    # reshape to array, scale if needed
    turb_arr = sim_turb_df.values.reshape(-1, ny, nz)
    if c == 'u':
        scale = np.std(turb_arr) / (tint * U)
    turb_arr = (turb_arr - np.mean(turb_arr, axis=0)) / scale + np.mean(turb_arr, axis=0)

    # save that turbulence component box in HAWC2 format
    bin_path = turb_path + f'_nocon_s{con_seed}_{c}.bin'
    with open(bin_path, 'wb') as bin_fid:
        turb_arr.astype(np.dtype('<f')).tofile(bin_fid)
    kwargs['seed'] = kwargs['seed'] + 1  # make sure u, v, w are not correlated
