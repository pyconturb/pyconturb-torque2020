# -*- coding: utf-8 -*-
"""'Measure' the generated fine-mesh box to generate the constraint files
"""
import matplotlib.pyplot as plt
from vtmeas import MetMast, TurbBox
from vtmeas.instruments import SonicAnemometer
from _inputs import (y_mid, z_mid, ny, nz, dx, dy, dz, U, f_samp, con_dir)


# script inputs
from _inputs import patterns, turb_path

# load the turbulence box
tb = TurbBox().read_hawc2(turb_path + '_{c}.bin', ny=ny, nz=nz, dx=dx, dy=dy, dz=dz,
                          y_mid=y_mid, z_mid=z_mid, u_adv=U)

# create constraint files from all of the patterns
for ip, pk in enumerate(patterns):
    z_sonics = patterns[pk]
    sonic_anems = [SonicAnemometer([0, 0, z], f'{z}m SonAnem', f_samp) for z in z_sonics]
    mm = MetMast(z_sonics[-1], instruments=sonic_anems)
    con_df = mm.measure(tb)
    con_df.to_csv(con_dir + f'/con_{pk}.csv')
