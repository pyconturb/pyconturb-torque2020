# -*- coding: utf-8 -*-
"""Generate the fine-mesh turbulence to be "measured"
"""
from pathlib import Path
import numpy as np
from pyconturb import gen_turb, gen_spat_grid
# from _t2020_utils import gen_mann_turb


# script inputs for, e.g., tbox size and turbulence settings
from _inputs import (U, T, nx, ny, nz, seed, zhub, alpha, turb_class, nf_chunk,
                     y, z, tint, turb_path)

# ---------------------------------------------------------------------------------------

# Zhu Li, do the thing
kwargs = {'u_ref': U, 'T': T, 'dt': T/nx, 'seed': seed,
          'z_ref': zhub, 'alpha': alpha, 'turb_class': turb_class,
          'coh_model': '3d', 'nf_chunk': nf_chunk}

# loop through components
for ic, c in enumerate('uvw'):
    spat_df = gen_spat_grid(y, z, comps=[ic])

    # simulate turbulence
    sim_turb_df = gen_turb(spat_df, **kwargs)

    # reshape to array, scale if needed
    turb_arr = sim_turb_df.values.reshape(-1, ny, nz)
    if c == 'u':
        scale = np.std(turb_arr) / (tint * U)
    turb_arr = (turb_arr - np.mean(turb_arr, axis=0)) / scale + np.mean(turb_arr, axis=0)

    # save that turbulence component box in HAWC2 format
    bin_path = turb_path + f'_{c}.bin'
    with open(bin_path, 'wb') as bin_fid:
        turb_arr.astype(np.dtype('<f')).tofile(bin_fid)
    kwargs['seed'] = kwargs['seed'] + 1  # make sure u, v, w are not correlated