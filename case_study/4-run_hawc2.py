# -*- coding: utf-8 -*-
"""Create HAWC2 htc files and run them
"""
from pathlib import Path
from _inputs import (turb_dir, old_10mw_path, htc_dir)
from _t2020_utils import gen_10mw_htc, run_hawc2


turb_paths = [str(f).replace('_u.bin', '') for f in Path(turb_dir).iterdir()
              if '_u.bin' in str(f)]

for p in turb_paths:
    tname = Path(p).name
    new_path = htc_dir + '/' + tname + '.htc'
    gen_10mw_htc(old_10mw_path, new_path, p)
    run_hawc2(new_path, cwd=Path('../simulations').absolute())
