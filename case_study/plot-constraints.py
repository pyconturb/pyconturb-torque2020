# -*- coding: utf-8 -*-
"""
Created on Tue Jan 21 10:37:33 2020

@author: rink
"""
import matplotlib.pyplot as plt
import numpy as np
from _inputs import (wid_m1, patterns, zhub, fig_dir, names)

#%%

nplots = len(patterns)
pltprms = {'font.size': 11, 'axes.labelsize': 13}
with plt.rc_context(pltprms):
    fig, axs = plt.subplots(1, nplots, num=2, clear=True, figsize=(9, 2.5))
for ip, pk in enumerate(patterns):
    axs[ip].plot(wid_m1/2*np.cos(np.linspace(0, 2*np.pi, 201)),
                wid_m1/2*np.sin(np.linspace(0, 2*np.pi, 201)) + zhub, 'k:',
                label='Rotor')
    axs[ip].plot(np.zeros(len(patterns[pk])), patterns[pk], 'o', mec='#ff7f0e',
                 ms=9, mfc='none', mew=2, alpha=0.8)
    axs[ip].axis('equal')
    axs[ip].set_title(names[pk])
    if ip > 0:
        axs[ip].set_yticklabels([])
plt.tight_layout()
fig.savefig(fig_dir + '/constraints.png', dpi=150)
