# -*- coding: utf-8 -*-
"""Sanity check: verify the turbulence box dimensions are right

Do this by comparing the turbulence box directly with the output from a HAWC2 simulation.
"""
import matplotlib.pyplot as plt
from vtmeas import TurbBox
from wetb.hawc2.Hawc2io import ReadHawc2
from _t2020_utils import gen_turb_only, run_hawc2

# script inputs
from _inputs import (ht, zhub, nx, ny, nz, dx, dy, dz, U, l, ae, gamma, alpha, hf_comp,
                     seed, turb_path, tint, z_mid, old_path)

# ---------------------------------------------------------------------------------------
# intermediate variables
new_path = '../simulations/htc/DTU_10MW_RWT_1s.htc'

# create htc file
gen_turb_only(old_path, new_path, turb_path, hf_comp=hf_comp, wsp=U, tint=tint)

# run hawc2
run_hawc2(new_path, cwd='../simulations')

# load the turbulence box and isolate the three points
tb = TurbBox().read_hawc2(turb_path + '_{c}.bin', ny=ny, nz=nz, dx=dx, dy=dy, dz=dz,
                          y_mid=dy/2, z_mid=z_mid, u_adv=U)
iy, iz = (tb.y.size-1)//2, (tb.z.size-1)//2
t = tb.t
u = tb.u[:, iy, [0, iz, -2]]  # we're not using the last row
v = tb.v[:, iy, [0, iz, -2]]
w = tb.w[:, iy, [0, iz, -2]]

# load the hawc2 results
h2res = ReadHawc2('../simulations/res/dtu_10mw_rwt_1s')
h2data = h2res.ReadBinary()
t_h2 = h2data[:, 0]
u_h2 = h2data[:, 2::3]
v_h2 = h2data[:, 1::3]
w_h2 = -h2data[:, 3::3]  # w and z are in opposite directions


# plot the comparisons
fig, axs = plt.subplots(3, 3, num=1, figsize=(19, 9), clear=True)
for i in range(3):
    wsp = [u, v, w][i]
    wsp_h2 = [u_h2, v_h2, w_h2][i]
    for j in range(3):
        axs[i, j].plot(t, wsp[:, j], label='TBox')
        axs[i, j].plot(t_h2, wsp_h2[:, j], label='HAWC2')
        axs[i, j].set_title(f'{"uvw"[i]}, {[29, 119, 209][j]} m')
        axs[i, j].set_xlim([0, 600])
        axs[i, j].grid('on')
axs[0, 0].legend()
plt.tight_layout()
#fig.savefig('../figures/sanity-check_fine_turbulence.png', dpi=100)
