# -*- coding: utf-8 -*-
"""
Created on Tue Jan 21 09:38:46 2020

@author: rink
"""
from pathlib import Path
import matplotlib.pyplot as plt
import numpy as np
from _inputs import (turb_dir, patterns, con_seed, fig_dir, times, names)
from wetb.hawc2.Hawc2io import ReadHawc2

turb_paths = [str(f).replace('_u.bin', '') for f in Path(turb_dir).iterdir()
              if '_u.bin' in str(f)]
res_paths = [s.replace('turb', 'res') for s in turb_paths]
i_time, i_rot, i_pow = 0, 9, 105
i_wsp_bot, i_wsp_hh, i_wsp_top = 14, 17, 20
i_brm1, i_brm2, i_brm3 = 25, 28, 31
i_tbfa, i_tbss = 22, 23
labels = ['Fine', 'No constraint', 'p0', 'p1', 'p2', 'p3']
i_plots = [i_rot]
title = 'Rotor speed'
#i_plots = [(i_rot, 'Rotor speed [rpm]'), (i_pow, 'Power [MW]')]
scales = [1, 1/1e6]

# %% load the data

p = [s for s in res_paths if s.endswith(f'nocon_s{con_seed}')][0]
nocon_data = ReadHawc2(p).ReadBinary()[:, [i_time] + i_plots]
p = [s for s in res_paths if s.endswith('a0.2')][0]
fine_data = ReadHawc2(p).ReadBinary()[:, [i_time] + i_plots]
con_data = np.empty((len(patterns),) + fine_data.shape)
for ic, pk in enumerate(sorted(patterns)):
    p = [s for s in res_paths if s.endswith(f'{pk}_s{con_seed}')][0]
    con_data[ic] = ReadHawc2(p).ReadBinary()[:, [i_time] + i_plots]
aae_nocon = np.mean(np.abs(nocon_data[:, 1] - fine_data[:, 1]))
aae_con = np.mean(np.abs(con_data[:, :, 1] - fine_data[:, 1]), axis=1)
print('AAE values: ')
print(f'No con: {aae_nocon:.3f}')
[print(f'Con p{i}: {aae_con[i]:.3f}') for i in range(5)]

# %% make the plot

fig, axs = plt.subplots(2, 1, num=4, clear=True, figsize=(8.5, 4.5))
for i, i_plot in enumerate(i_plots):
    scale = scales[i]
    # constraints
    for ic, pk in enumerate(sorted(patterns)):
        axs[2*i].plot(con_data[ic, :, 0], scale*con_data[ic, :, i+1],
                    alpha=0.7, label=names[pk])
        axs[2*i+1].plot(con_data[ic, :, 0],
                    scale*(con_data[ic, :, i+1] - fine_data[:, i+1]),
                    alpha=0.7, label=names[pk])
    # no-constraint
    axs[2*i].plot(nocon_data[:, 0], scale*nocon_data[:, i+1], ':', c='0.1',
                   alpha=0.8, label='No con.')
    axs[2*i+1].plot(nocon_data[:, 0], scale*(nocon_data[:, i+1]
                                            - fine_data[:, i+1]),
                   ':', c='0.1', alpha=0.8, label='No con.')
    # original signal
    axs[2*i].plot(fine_data[:, 0], scale*fine_data[:, i+1], '--', c='k',
                   alpha=0.7, label='Original')
    # times
    for isb in range(2):
        ylim = axs[2*i+isb].get_ylim()
        [axs[2*i+isb].plot([times[it], times[it]], ylim, '0.8', lw=2, zorder=1)
         for it in range(len(times))]
        axs[2*i+isb].set_ylim(ylim)
    # prettify
    axs[2*i].set_xticklabels([])
    axs[2*i].set_title('Rotor speed [rpm]')
    axs[2*i+1].set_title('Rotor speed error [rpm]')
    for isb in range(2):
        axs[2*i+isb].set_xlim([150, 650])
        axs[2*i+isb].grid('on')
axs[0].legend(bbox_to_anchor=(1.02, 1.0, 1, 0.05), loc='upper left')
axs[1].set_xlabel('Time [s]')
plt.tight_layout()
for isl in range(len(times)):
    axs[1].annotate('ABCDEFG'[isl], (times[isl], ylim[1]),
                    xytext=(-15, -12), textcoords='offset points', ha='center', va='center',
                    fontsize=11, bbox=dict(facecolor='w', edgecolor='0.1', alpha=0.7,
                                           boxstyle='round'))
fig.savefig(fig_dir + '/case_study_comparison.png', dpi=200)
