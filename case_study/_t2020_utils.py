# -*- coding: utf-8 -*-
"""Useful functions for torque 2020 paper
"""
from pathlib import Path
import subprocess
from wetb.hawc2 import HTCFile
from _inputs import (dx, dy, dz, U, tint, l, ae, gamma, seed, hf_comp, nx, ny, nz)


def gen_mann_turb(turb_path='test', ae=1.0, l=29.4, gamma=3.0, seed=1337, nx=64, ny=32,
                  nz=32, dx=2, dy=5, dz=5, hf_comp=True):
    """Call the standalone Mann turbulence generator


    Parameters
    ----------
    turb_path : str
        Path (no extension) to where you want the turbulence files to be generated.
        Executable will create four files, three .bin  files and one summary txt file.
    other parameters :  miscellaneous
        Directly correspond to Mann parameters in HAWC2 block.
    """
    # location of mann executable
    exe_path = 'C:/Users/rink/Documents/hawc2/mann_turb_x64/mann_turb_x64.exe'
    # run the executable
    inputs = [turb_path, ae, l, gamma, seed, nx, ny, nz, dx, dy, dz, hf_comp]
    res = subprocess.run([exe_path] + [str(i) for i in inputs],
                         stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    if res.returncode:
        print('Mann turbulence generator exited with error! Output:')
        print(res.stderr)


def gen_turb_only(old_path, new_path, turb_path, hf_comp=True, wsp=12, tint=0.1):
    """Make a turbulence-only htc file from LAC course DTU 10 MW htc file.
    NOTE!!! This assumes turb_path is formatted in a specific way.
    """
    # extract turbulence parameters from filename
    turb_name = Path(turb_path).name
    split_turb = turb_name.split('_')
    ae, l, gamma = 1, 29.4, 3
    nx, ny, nz = [int(s) for s in split_turb[1].split('x')]
    dx, dy, dz = [float(s) for s in split_turb[2].split('x')]
    seed = int(split_turb[3].lstrip('s'))
    hf_comp = [0, 1][hf_comp]  # convert True/False to integer
    # htc files
    new_name = Path(new_path).name.replace('.htc', '')
    htc = HTCFile(old_path)
    # simulation block
    htc.simulation.time_stop = 600
    htc.simulation.logfile = f'./log/{new_name}.log'
    # no body to simulate
    del htc.new_htc_structure
    # wind block
    htc.wind.wsp = wsp
    htc.wind.tint = tint
    htc.wind.tower_shadow_method = 0
    htc.wind.center_pos0 = [-dy/2, 0, -119-dz/2]  # offset so center is hh
    htc.wind.shear_format = [0, 0]
    htc.wind.turb_format = 1
    # add mann subblock
    htc.wind.add_section('mann')
    htc.wind.mann.create_turb_parameters = [l, ae, gamma, seed, hf_comp]
    htc.wind.mann['filename_u'] = f'{turb_path}_u.bin'
    htc.wind.mann['filename_v'] = f'{turb_path}_v.bin'
    htc.wind.mann['filename_w'] = f'{turb_path}_w.bin'
    htc.wind.mann['box_dim_u'] = [nx, dx]
    htc.wind.mann['box_dim_v'] = [ny, dy]
    htc.wind.mann['box_dim_w'] = [nz, dz]
    htc.wind.mann['dont_scale'] = [1]
    # no simulation
    del htc.wind.wind_ramp_factor
    del htc.wind.tower_shadow_potential_2
    del htc.aerodrag
    del htc.aero
    del htc.dll
    # brief output block
    del htc.output
    htc.add_section('output')
    htc.output.filename = f'./res/{new_name}'
    htc.output.data_format = 'hawc_binary'
    htc.output.buffer = 1
    htc.output.general = 'time'
    htc.output.add_line('wind', ['free_wind', 1, 0.0, 0.0, -29, '# bottom'])
    htc.output.add_line('wind', ['free_wind', 1, 0.0, 0.0, -119, '# hub height'])
    htc.output.add_line('wind', ['free_wind', 1, 0.0, 0.0, -209, '# bottom'])
    htc.save(new_path)


def gen_10mw_htc(old_path, new_path, turb_path):
    """HTC file for aeroelastic simulation of 10 MW"""
    # extract turbulence parameters from filename
#    turb_name = Path(turb_path).name
#    split_turb = turb_name.split('_')
#    ae, l, gamma = [float(s.lstrip('laeg')) for s in split_turb[:3]]
#    nx, ny, nz = [int(s) for s in split_turb[3].split('x')]
#    dx, dy, dz = [float(s) for s in split_turb[4].split('x')]
#    seed = int(split_turb[5].lstrip('s'))
    hf_comp = 1  # convert True/False to integer
    # htc files
    new_name = Path(new_path).name.replace('.htc', '')
    htc = HTCFile(old_path)
    # simulation block
    htc.simulation.logfile = f'./log/{new_name}.log'
    # wind block
    htc.wind.wsp = U
    htc.wind.tint = tint
    htc.wind.tower_shadow_method = 3  # 2nd potential flow
    htc.wind.center_pos0 = [-dy/2, 0, -119-dz/2]  # offset so center is hh
    htc.wind.shear_format = [0, 0]
    htc.wind.turb_format = 1
    # add mann subblock
#    htc.wind.add_section('mann')
    htc.wind.mann.create_turb_parameters = [l, ae, gamma, seed, hf_comp]
    htc.wind.mann.filename_u = f'{turb_path}_u.bin'
    htc.wind.mann.filename_v = f'{turb_path}_v.bin'
    htc.wind.mann.filename_w = f'{turb_path}_w.bin'
    htc.wind.mann.box_dim_u = [nx, dx]
    htc.wind.mann.box_dim_v = [ny, dy]
    htc.wind.mann.box_dim_w = [nz, dz]
    htc.wind.mann.dont_scale = [1]
    # update output block
    htc.output.filename = f'./res/{new_name}'
    htc.save(new_path)


def run_hawc2(htc, cwd='.'):
    """call hawc2 on an htc file"""
    cwd = Path(cwd).absolute().as_posix()
    htc = Path(htc).absolute().as_posix()
    proc = subprocess.run(['C:/Users/rink/Documents/hawc2/HAWC2_all_12.6/HAWC2MB.exe',
                           htc], cwd=cwd, stdout=subprocess.PIPE,
                          stderr=subprocess.STDOUT)
    if proc.returncode:
        print('HAWC2 failed!!! Error message:\n')
        print(proc.stdout.decode('utf-8'))
