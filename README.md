# pyconturb-torque2020

Code and simulations for Torque 2020 PyConTurb paper: "Uncertainty in loads for different constraint patterns in constrained-turbulence generation."

Required installation: [PyConTurb](https://gitlab.windenergy.dtu.dk/pyconturb/pyconturb) and [VTMeas](https://gitlab.windenergy.dtu.dk/pyconturb/VTMeas).

The code for the case study is in folder `case_study`.

The code for the numerical experiment is in folder `experiment_1024x16x16/`.
